%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parse.tab.h"
%}

%option nounistd 

/*Tipos de dados*/
ALPHA			[A-Za-z_]
DIGIT			[0-9]
ALPHANUM		[A-Za-z_0-9]+({SPECIAL_CARACTER}+)?
STRING			{ALPHANUM}+({SPECIAL_CARACTER}+)?
INTEGER			{DIGIT}+
FLOAT			[\-]?{DIGIT}+"."{DIGIT}+([Ee][\+\-]?{DIGIT}+)?
SPECIAL_CARACTER [\,\;\:\(\)\[\]\{\}\+\-\*\/\<\>\=\!\&\$\%\#\^\_]

/*Componentes*/
RESISTOR		[Rr]{ALPHANUM}+
CAPACITOR		[Cc]{ALPHANUM}+
INDUCTOR		[Ll]{ALPHANUM}+
CURRSRC			[Ii]{ALPHANUM}+
VOLTSRC			[Vv]{ALPHANUM}+

/*Fontes Controladas*/
VCVS			[Ee]{ALPHANUM}+
CCCS			[Ff]{ALPHANUM}+
VCCS			[Gg]{ALPHANUM}+
CCVS			[Hh]{ALPHANUM}+

/*Operacao*/
OPER			"."+[oO][pP]

/*Elementos especiais*/
DIODO			[Dd]{ALPHANUM}+
TJB				[Qq]{ALPHANUM}+
MOSFET			[Mm]{ALPHANUM}+

EOL			[\n]
DELIMITER		[ \t]+
UNIT			[Ff]|[Pp]|[Nn]|[Uu]|[Mm]|[Kk]|[Mm][Ee][Gg]|[Gg]|[Tt]
VALUE			({FLOAT}|[-]?{INTEGER}){UNIT}
COMMENT			[\*][^\n]+

%%

{INTEGER}		{yylval.n = atoi(yytext); return INTEGER;}
{FLOAT}			{yylval.d = atof(yytext); return FLOAT;}
{RESISTOR}		{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return RESISTOR;
				}
{CAPACITOR}		{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return CAPACITOR;
				}
{INDUCTOR}		{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return INDUCTOR;
				}
{CURRSRC}		{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return CURRSRC;				
				}
{VOLTSRC}		{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return VOLTSRC;
				}
{VCCS}			{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return VCCS;
				}

{VCVS}			{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return VCVS;
				}

{CCCS}			{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return CCCS;
				}

{CCVS}			{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return CCVS;
				}

{DIODO}			{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return DIODO;
				}

{TJB}			{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return TJB;
				}

{MOSFET}		{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 strcpy(yylval.s, yytext);
				 yylval.s[strlen(yytext)] = '\0';
                 		return MOSFET;
				}

{OPER}			{
					yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				 	strcpy(yylval.s, yytext);
				 	yylval.s[strlen(yytext)] = '\0';                	
                 	return OPER;
				}


{VALUE}			{double value; int len; char u;
				 value = atof(yytext);
				 len = strlen(yytext);
				 u = yytext[len - 1];
				 if (u == 'F' || u == 'f')
					yylval.d = value * 1e-15;
				else if (u == 'P' || u == 'p')
					yylval.d = value * 1e-12;
				 else if (u == 'N' || u == 'n')
					yylval.d = value * 1e-9;
				 else if (u == 'U' || u == 'u')
					yylval.d = value * 1e-6;
				 else if (u == 'M' || u == 'm')
					yylval.d = value * 1e-3;
				 else if (u == 'K' || u == 'k')
					yylval.d = value * 1e3;
				 else if (u == 'G' || u == 'g')
			     {
					if (yytext[len - 2] == 'E' | yytext[len - 2] == 'e')
						yylval.d = value * 1e6;
					else
						yylval.d = value * 1e9;
				 }
				 else if (u == 'T' | u == 't')
					yylval.d = value * 1e12;
				 else
					yylval.d = value;
				 return VALUE;
				}
{STRING}		{yylval.s = (char*)malloc((strlen(yytext) + 1) * sizeof(char)); 
				strcpy(yylval.s, yytext);
				yylval.s[strlen(yytext)] = '\0';
				return STRING;
				}

{EOL}			{return EOL;}
<<EOF>>			{ return EOF;}
{DELIMITER}		{ }
{COMMENT}		{return EOL;}
{SPECIAL_CARACTER} {return yytext[0];}

%%

int yywrap()
{
    return  1;
}
