#ifdef MATLAB
#include "mex.h"
#endif

#include "parse_func.h"
#include "Symbol_Table.h"
#include <string.h>

//Contadores
int nRes;
int nCap;
int nInd;
int nVsrc;
int nIsrc;
int nVCCS;
int nVCVS;
int nCCCS;
int nCCVS;
int nDiodo;
int nTjb;
int nMosfet;

void Init_parse_util()
{
	nRes = 0;
	nCap = 0;
	nInd = 0;
	nVsrc = 0;
	nIsrc = 0;
	nVCCS = 0;
	nVCVS = 0;
	nCCCS = 0;
	nCCVS = 0;
	nDiodo = 0;
	nTjb = 0;
	nMosfet = 0;
}


void ParseRes(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 0;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));
	
	printf("RES[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);

	nRes++;

	nodelist = malloc(sizeof(Node_Entry*) * numnodes);

	nodelist[0] = Insert_Node_Entry(node1);
	nodelist[1] = Insert_Node_Entry(node2);

	Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseCap(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 1;
	Node_Entry **nodelist;
	
	name = strndup(name, strlen(name));

	printf("CAP[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nCap++;

	//nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseInd(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 2;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));
	
	printf("IND[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nInd++;

	//Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseVsrc(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 3;
	Node_Entry **nodelist;
	
	name = strndup(name, strlen(name));

	printf("VSRC[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nVsrc++;

	//Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseIsrc(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 4;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));
	
	printf("ISRC[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nIsrc++;

	// Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseVCCS(char *name, char *node1, char *node2, char *node3, char *node4, double value)
{
	int numnodes = 4;
	int tipo = 5;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));

	printf("VCCS[%s];\t n+[%s];\t Ne-[%s];\t Nc+[%s];\t Nc-[%s];\t valor[%e]\n", name, node1, node2, node3, node4, value);

	nVCCS++;

	// Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    nodelist[3] = Insert_Node_Entry(node4);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseVCVS(char *name, char *node1, char *node2, char *node3, char *node4, double value)
{
	int numnodes = 4;
	int tipo = 6;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));
	
	printf("VCVS[%s];\t n+[%s];\t Ne-[%s];\t Nc+[%s];\t Nc-[%s];\t valor[%e]\n", name, node1, node2, node3, node4, value);

	nVCVS++;

	// Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    nodelist[3] = Insert_Node_Entry(node4);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseCCCS(char *name, char *node1, char *node2, char *elem_control, double value)
{
	int numnodes = 2;
	int tipo = 7;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));
	
	printf("CCCS[%s];\t n+[%s];\t Ne-[%s];\t Elem_Control[%s];\t valor[%e]\n", name, node1, node2, elem_control, value);

	nCCCS++;

	// Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseCCVS(char *name, char *node1, char *node2, char *node3, char *node4, double value)
{
	int numnodes = 4;
	int tipo = 8;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));
	
	printf("CCVS[%s];\t n+[%s];\t Ne-[%s];\t Nc+[%s];\t Nc-[%s];\t valor[%e]\n", name, node1, node2, node3, node4, value);

	nCCVS++;

	//Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    nodelist[3] = Insert_Node_Entry(node4);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseDiodo(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 9;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));
	
	printf("DIODO[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nDiodo++;

	// Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseTjb(char *name, char *node1, char *node2, char *node3, double value)
{
	int numnodes = 3;
	int tipo = 10;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));

	printf("TJB[%s];\t nE[%s];\t nC[%s];\t nB[%s];\t valor[%e]\n", name, node1, node2, node3, value);
	nTjb++;

	// Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseMosfet(char *name, char *node1, char *node2, char *node3, double value)
{
	int numnodes = 3;
	int tipo = 11;
	Node_Entry **nodelist;

	name = strndup(name, strlen(name));

	printf("Mosfet[%s];\t nE[%s];\t nC[%s];\t nB[%s];\t valor[%e]\n", name, node1, node2, node3, value);
	nMosfet++;

	// Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void makeCircuit(char *function)
{
	printf(function);
	printf(" <<make operation point for the circuit>>");
}

void Summarize()
{
	printf("\n\nTotal:\n   #Res=%d, #Cap=%d, #Ind=%d, #Vsrc=%d, #Isrc=%d, #VCVS=%d, #CCCS=%d, #VCCS=%d, #CCVS=%d, #Diodo=%d, #TJB=%d, #Mosfet=%d   \n", \
		nRes, nCap, nInd, nVsrc, nIsrc, nVCVS, nCCCS, nVCCS, nCCVS, nDiodo, nTjb, nMosfet);
	printf("\n\nFim..............]\n");


	//printf("Imprime a device table...");
	//Print_Device_Table();

}